#ifndef FILEACCESS_H
#define FILEACCESS_H

#include "../fileinterface.h"
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using std::string;
using std::ifstream;
using std::vector;

class FileAccess : public FileInterface {
    ifstream fin;
    string filename;
    int size;
    int separator;

    int i, len;
    vector<unsigned char> buf;
    vector<int> z;

public:
    FileAccess() {}
    virtual bool Open_file(string filename, int size, int separator) override;
    virtual bool Get_number(int &number) override;
    virtual void Close_file() override;
};

#endif // FILEACCESS_H
