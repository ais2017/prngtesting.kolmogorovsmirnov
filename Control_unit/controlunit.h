#ifndef CONTROLUNIT_H
#define CONTROLUNIT_H
#include "../Buisness_objects/sequence.h"
#include "../Buisness_objects/test.h"
#include "controlinterface.h"
#include "databaseinterface.h"
#include "fileinterface.h"
#include <vector>
#include <map>
#include <string>
#include <utility>

using std::vector;
using std::map;
using std::string;
using std::pair;

class ControlUnit: public ControlInterface {
    unsigned int index;             //amount of tests was created
    vector<Sequence> seqs;          //loaded sequences
    map<unsigned int, Test> tests;  //loaded or created tests

    DatabaseInterface *dbinterface;
    FileInterface *flinterface;


public:
    ControlUnit(unsigned int index, DatabaseInterface *dbinterface, FileInterface *flinterface);

    //Return false if wrong size or separator
    //size - amount of bits in the sequence numbers, 0 < size <= 16
    //separator - amount of bits in separation symbols, 0 <= separator
    virtual bool Check_params(int size, int separator) override;

    //Return false, if wrong file name; load sequence from file
    virtual bool Load_sequence(string file, int size, int separator) override;

    //Return false, if wrong signlevel, 0 < signlevel < 1
    //Create and show test with sequences was loading before, after that clear sequences
    virtual bool Build_test(double signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) override;

    //Return false, if no such test
    //Search and show information about test with appropriate index
    virtual bool Show_test(unsigned int index, double &signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) override;

    //Return false, if no test in database or in loaded tests
    //Searching test in database and in loaded tests and return it to test
    bool Search_test(unsigned int index, Test &test);

    friend class ControlUnitTest;
};

#endif // CONTROLUNIT_H
