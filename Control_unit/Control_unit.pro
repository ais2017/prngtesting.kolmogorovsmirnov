QT += core
QT -= gui

TARGET = Control_unit
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app

INCLUDEPATH += "../Buisness_objects"

SOURCES += \
    controlunit.cpp \
    "../Buisness_objects/*.cpp" \
    prngksmodule.cpp \
    FileAccess/fileaccess.cpp \
    "./testControlUnit/testinterfaces.cpp"

HEADERS += \
    controlunit.h \
    controlinterface.h \
    databaseinterface.h \
    fileinterface.h \
    "../Buisness_objects/*.h" \
    FileAccess/fileaccess.h \
    "./testControlUnit/testinterfaces.h"

