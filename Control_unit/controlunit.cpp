#include "controlunit.h"

ControlUnit::ControlUnit(unsigned int index, DatabaseInterface *dbinterface, FileInterface *flinterface):
    index(index), dbinterface(dbinterface), flinterface(flinterface) {}

bool ControlUnit::Check_params(int size, int separator) {
    return (0 < size && size <= 16 && 0 <= separator)? 1: 0;
}

bool ControlUnit::Load_sequence(string file, int size, int separator) {
    //Open file with sequence
    if (!ControlUnit::flinterface->Open_file(file, size, separator))
        return 0;

    //Load sequences
    Sequence seq(size);
    int num = 0;
    while (ControlUnit::flinterface->Get_number(num))
        seq.add(num);
    seq.buildGraph();
    ControlUnit::seqs.push_back(seq);

    //Close file
    ControlUnit::flinterface->Close_file();

    return 1;
}

bool ControlUnit::Build_test(double signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) {
    //Check signlevel correctness
    if (signlevel <= 0 || signlevel >= 1)
        return 0;

    //Create new test with next index from ControlUnit::index
    Test tst(signlevel);
    for (unsigned int i = 0; i < ControlUnit::seqs.size(); ++i)
        tst.addSequence(ControlUnit::seqs[i]);
    tst.recalcStats();
    ControlUnit::tests.insert(pair<unsigned int, Test>(ControlUnit::index, tst));
    //Save test
    ControlUnit::dbinterface->Save_test(ControlUnit::index, tst);
    //Amount of tests increament
    ++(ControlUnit::index);
    //Clear sequences
    ControlUnit::seqs.clear();

    //Return values
    signlevel = tst.getSignLevel();
    signvalue = tst.getSignValue();
    stats = tst.getStats();

    //Create and return graph
    graph.clear();
    graph.resize(tst.getSequences().size());
    for (unsigned int i = 0; i < tst.getSequences().size(); ++i)
        graph[i] = tst.getSequence(i).getGraph();

    return 1;
}

bool ControlUnit::Show_test(unsigned int index, double &signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) {
    Test tst;
    //Seqrch test
    if (!ControlUnit::Search_test(index, tst))
        return 0;

    //Return values
    signlevel = tst.getSignLevel();
    signvalue = tst.getSignValue();
    stats = tst.getStats();

    //create and return graph
    graph.clear();
    graph.resize(tst.getSequences().size());
    for (unsigned int i = 0; i < tst.getSequences().size(); ++i)
        graph[i] = tst.getSequence(i).getGraph();

    return 1;
}

bool ControlUnit::Search_test(unsigned int index, Test &test) {
    //Search test in map of loading tests
    if (ControlUnit::tests.find(index) != ControlUnit::tests.end()) {
        test = ControlUnit::tests[index];
        return true;
    }

    //Search test in database
    if (ControlUnit::dbinterface->Search_test(index, test)) {
        ControlUnit::tests.insert(pair<unsigned int, Test>(index, test));
        return true;
    }

    return false;
}

