#ifndef DATABASEINTERFACE_H
#define DATABASEINTERFACE_H

#include "../Buisness_objects/test.h"


class DatabaseInterface {
public:
    DatabaseInterface() {}
    virtual unsigned int Get_last_index() = 0;
    virtual bool Search_test(unsigned int index, Test &test) = 0;
    virtual void Save_test(unsigned int index, const Test &test) = 0;
};

#endif // DATABASEINTERFACE_H
