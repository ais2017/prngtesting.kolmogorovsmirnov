#include <QString>
#include <QtTest>

#include <iostream>
#include "../controlunit.h"
#include "../../Buisness_objects/sequence.h"
#include "../../Buisness_objects/test.h"
#include "testinterfaces.h"

class ControlUnitTest : public QObject
{
    Q_OBJECT

    bool seqequal(Sequence seq1, Sequence seq2);
    bool testequal(Test tst1, Test tst2);

public:
    ControlUnitTest();

private Q_SLOTS:
    void testCheck_params();
    void testLoad_sequence();
    void testBuild_test();
    void testSearch_test();
    void testShow_test();
};

ControlUnitTest::ControlUnitTest() {}

void ControlUnitTest::testCheck_params() {
    testFileInterface fli;
    testDatabaseInterface dbi;
    ControlInterface *cu = new ControlUnit(0, &dbi, &fli);

    QVERIFY2(cu->Check_params(-1, 5) == 0, "Low size");
    QVERIFY2(cu->Check_params(0, 3) == 0, "Zero size");
    QVERIFY2(cu->Check_params(4, 197) == 1, "Good params");
    QVERIFY2(cu->Check_params(16, 21) == 1, "Good params, max size");
    QVERIFY2(cu->Check_params(100, 8) == 0, "High size");
    QVERIFY2(cu->Check_params(8, -3) == 0, "Low separator");
    QVERIFY2(cu->Check_params(9, 0) == 1, "Zero separator");
    QVERIFY2(cu->Check_params(-1, -5) == 0, "Low both size and separator");
    QVERIFY2(cu->Check_params(56, -7) == 0, "Low separator with high size");
}

void ControlUnitTest::testLoad_sequence() {
    testFileInterface fli;
    testDatabaseInterface dbi;
    ControlInterface *cu = new ControlUnit(0, &dbi, &fli);

    QVERIFY2(cu->Load_sequence("bad", 7, 2) == 0, "Bad file was given");
    QVERIFY2(fli.filename == "bad" && fli.size == 7 && fli.separator == 2, "Check parameters bad file");

    QVERIFY2(cu->Load_sequence("ok", 4, 0) == 1, "Empty sequence return value");
    QVERIFY2(fli.filename == "ok" && fli.size == 4 && fli.separator == 0, "Check parameters empty sequence");
    Sequence seq(4);
    seq.buildGraph();
    QVERIFY2(seqequal(static_cast<ControlUnit *>(cu)->seqs[0], seq), "Empty sequence");

    fli.data = {1, 0, 0, 1, 5, 15, 3, 9, 13, 13, 7, 13, 3, 12, 0, 1, 11, 7, 6, 9, 13, 13, 6, 4, 7, 4, 7, 4, 3};
    fli.i = 0;
    for (unsigned int i = 0; i < fli.data.size(); ++i)
        seq.add(fli.data[i]);
    seq.buildGraph();
    QVERIFY2(cu->Load_sequence("ok", 4, 0) == 1, "Non empty sequence return value");
    QVERIFY2(fli.filename == "ok" && fli.size == 4 && fli.separator == 0, "Check parameters");
    QVERIFY2(seqequal(static_cast<ControlUnit *>(cu)->seqs[1], seq), "Non empty sequence");
}

void ControlUnitTest::testBuild_test() {
    testFileInterface fli;
    testDatabaseInterface dbi;
    ControlInterface *cu = new ControlUnit(0, &dbi, &fli);
    double sv;
    vector<double> st;
    vector<vector<double> > gr;

    QVERIFY2(cu->Build_test(0.0, sv, st, gr) == 0, "Low signlevel");
    QVERIFY2(cu->Build_test(13.0, sv, st, gr) == 0, "High signlevel");

    fli.i = 0;
    fli.data = {1, 0, 0, 1, 5, 15, 3, 9, 13, 13, 7, 13, 3, 12, 0, 1, 11, 7, 6, 9, 13, 13, 6, 4, 7, 4, 7, 4, 3};
    cu->Load_sequence("ok", 4, 0);
    fli.i = 0;
    fli.data = {7, 3, 4, 0, 1, 2, 2, 2, 2, 2, 4, 12, 0, 0, 15, 14, 4, 9, 10, 10, 5, 12, 8, 9};
    cu->Load_sequence("ok", 4, 3);
    Test tst(0.1, static_cast<ControlUnit *>(cu)->seqs, vector<double>());
    tst.recalcStats();
    QVERIFY2(cu->Build_test(0.1, sv, st, gr) == 1, "Good signlevel");
    QVERIFY2(static_cast<ControlUnit *>(cu)->index == 1 && static_cast<ControlUnit *>(cu)->seqs.empty() &&
             static_cast<ControlUnit *>(cu)->tests.size() == 1, "Check variables");
    QVERIFY2(testequal(static_cast<ControlUnit *>(cu)->tests[0], tst), "Check test");
    QVERIFY2(testequal(tst, dbi.test) && dbi.index == 0, "Check saving in database");
    QVERIFY2(sv == tst.getSignValue() && st == tst.getStats(), "Compare testinfo");
    QVERIFY2(gr[0] == tst.getSequence(0).getGraph() && gr[1] == tst.getSequence(1).getGraph(), "Graph compare");
}

void ControlUnitTest::testSearch_test() {
    Sequence seq1(vector<int>({3, 7, 0, 22, 8, 44, 9, 12}), vector<double>());
    Sequence seq2(vector<int>({7, 1, 3, 9, 7, 12, 33, 9}), vector<double>());
    Sequence seq3(vector<int>({56, 34, 9, 2, 13, 67, 32, 10}), vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    seq3.buildGraph();
    vector<Sequence> seqs({seq1, seq2, seq3});
    Test tst(0.5, seqs, vector<double>());
    tst.recalcStats();

    testFileInterface fli;
    testDatabaseInterface dbi;
    ControlInterface *cu = new ControlUnit(0, &dbi, &fli);
    dbi.test = tst;

    Test tstr;
    QVERIFY2(static_cast<ControlUnit *>(cu)->Search_test(0, tstr) == 0, "No test in map and in database");

    QVERIFY2(static_cast<ControlUnit *>(cu)->Search_test(1, tstr) == 1, "Test only in database");
    QVERIFY2(testequal(tst, tstr) == 1, "Check test was given from database");
    QVERIFY2(static_cast<ControlUnit *>(cu)->tests.size() == 1 && testequal(static_cast<ControlUnit *>(cu)->tests[1], tst),
            "Check test from database was add into map");

    QVERIFY2(static_cast<ControlUnit *>(cu)->Search_test(1, tstr) == 1, "Test is in map");
    QVERIFY2(testequal(tst, tstr), "Check test from map");
}

void ControlUnitTest::testShow_test() {
    testFileInterface fli;
    testDatabaseInterface dbi;
    ControlInterface *cu = new ControlUnit(0, &dbi, &fli);
    double sl, sv;
    vector<double> st;
    vector<vector<double> > gr;

    fli.i = 0;
    fli.data = {1, 0, 0, 1, 5, 15, 3, 9, 13, 13, 7, 13, 3, 12, 0, 1, 11, 7, 6, 9, 13, 13, 6, 4, 7, 4, 7, 4, 3};
    cu->Load_sequence("ok", 4, 0);
    fli.i = 0;
    fli.data = {7, 3, 4, 0, 1, 2, 2, 2, 2, 2, 4, 12, 0, 0, 15, 14, 4, 9, 10, 10, 5, 12, 8, 9};
    cu->Load_sequence("ok", 4, 3);
    cu->Build_test(0.7, sv, st, gr);
    Test tst = static_cast<ControlUnit *>(cu)->tests[0];

    QVERIFY2(cu->Show_test(0, sl, sv, st, gr) == 1, "Test in map");
    QVERIFY2(sl == tst.getSignLevel() && sv == tst.getSignValue() && st == tst.getStats(), "Compare testinfo");
    QVERIFY2(gr[0] == tst.getSequence(0).getGraph(), "First graph line");
    QVERIFY2(gr[1] == tst.getSequence(1).getGraph(), "Second graph line");

    QVERIFY2(cu->Show_test(2, sv, sl, st, gr) == 1, "Test in database");
    QVERIFY2(cu->Show_test(7, sv, sl, st, gr) == 0, "No test");
}



bool ControlUnitTest::seqequal(Sequence seq1, Sequence seq2) {
    bool flag = (seq1.getSize() == seq2.getSize());
    if (flag) {
        for (unsigned int i = 0; i < seq1.getSize(); ++i)
            if (seq1[i] != seq2[i]) {
                flag = 0;
                std::cout << flag << std::endl;
            }
        flag = flag && (seq1.getGraph() == seq2.getGraph());
    }
    return flag;
}

bool ControlUnitTest::testequal(Test tst1, Test tst2) {
    bool flag = (tst1.getSignLevel() == tst2.getSignLevel()) && (tst1.getSignValue() == tst2.getSignValue());
    flag = flag && (tst1.getStats() == tst2.getStats());
    if (tst1.getSequences().size() == tst2.getSequences().size())
        for (unsigned int i = 0; i < tst1.getSequences().size(); ++i)
            flag = flag && seqequal(tst1.getSequence(i), tst2.getSequence(i));
    else
        flag = 0;
    return flag;
}


QTEST_APPLESS_MAIN(ControlUnitTest)

#include "tst_controlunittest.moc"
