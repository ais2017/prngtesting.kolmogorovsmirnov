#include "testinterfaces.h"

unsigned int testDatabaseInterface::Get_last_index() {
    return 0;
}

bool testDatabaseInterface::Search_test(unsigned int index, Test &test) {
    test = testDatabaseInterface::test;
    return (0 < index && index < 6);
}

void testDatabaseInterface::Save_test(unsigned int index, const Test &test) {
    testDatabaseInterface::index = index;
    testDatabaseInterface::test = test;
}

bool testFileInterface::Open_file(string filename, int size, int separator) {
    testFileInterface::filename = filename;
    testFileInterface::size = size;
    testFileInterface::separator = separator;
    if (filename == "ok")
        return 1;
    return 0;
}
bool testFileInterface::Get_number(int &number) {
    if (testFileInterface::i < testFileInterface::data.size()) {
        number = testFileInterface::data[testFileInterface::i];
        ++testFileInterface::i;
        return 1;
    }
    return 0;
}

void testFileInterface::Close_file() {}


