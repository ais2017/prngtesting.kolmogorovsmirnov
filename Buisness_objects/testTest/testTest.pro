#-------------------------------------------------
#
# Project created by QtCreator 2018-11-01T21:52:41
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_testtestclass
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11
LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LDFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

TEMPLATE = app

INCLUDEPATH += "../"
SOURCES += tst_testtestclass.cpp \
    ../sequence.cpp \
    ../test.cpp

HEADERS += \
    ../sequence.h \
    ../test.h
DEFINES += SRCDIR=\\\"$$PWD/\\\"
