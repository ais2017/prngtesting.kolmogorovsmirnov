#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <vector>
#include <exception>

using std::vector;

class Sequence {
    vector<int> data;       //Frequency of each number in input data stream
    vector<double> graph;   //Propotional frequency of each number in input data stream with accumulation

public:
    Sequence(unsigned int size);
    Sequence(vector<int> data, vector<double> graph);
    Sequence(const Sequence &seq);
    unsigned int getSize();
    vector<double> getGraph();
    void buildGraph();
    int operator[](unsigned int index);
    void add(int value);

    friend class TestSequenceclass;     //Test class
    friend class TestTestclass;         //Test class
};

class two_size_error: public std::exception {
    virtual const char* what() const throw() {
        return "Data size and graph size aren't equal";
    }
};

class sequence_index_error: public std::exception {
    virtual const char* what() const throw() {
        return "Access to nonexistent element";
    }
};

#endif // SEQUENCE_H
