#include "test.h"

Test::Test(double signlevel):signlevel(signlevel) {
    Test::signvalue = sqrt(log(Test::signlevel / 2.0) * (-0.5));
}

Test::Test(double signlevel, vector<Sequence> sequences, vector<double> statresults) {
    Test::signlevel = signlevel;
    Test::signvalue = sqrt(log(Test::signlevel / 2.0) * (-0.5));
    Test::sequences = sequences;
    Test::statresults = statresults;
}

double Test::getSignLevel() {
    return Test::signlevel;
}

double Test::getSignValue() {
    return Test::signvalue;
}

vector<double> Test::getStats() {
    return Test::statresults;
}

vector<Sequence> Test::getSequences() {
    return Test::sequences;
}

Sequence Test::getSequence(unsigned int index) {
    if (index >= Test::sequences.size()) {
        throw test_sequence_index_error();
        return Sequence(0);
    }
    return Test::sequences[index];
}

void Test::addSequence(const Sequence &seq) {
    Test::sequences.push_back(seq);
}

void Test::deleteSequence(unsigned int index) {
    if (index >= Test::sequences.size()) {
        throw test_sequence_index_error();
        return;
    }
    Test::sequences.erase(Test::sequences.begin() + index);
}

void Test::recalcStats() {
    for (unsigned int i = 0; i < Test::sequences.size(); ++i) {
        double max = 0;
        for (unsigned int j = i + 1; j < Test::sequences.size(); ++j)
            for (unsigned int k = 0; k < Test::sequences[i].getSize(); ++k) {
                double x = (Test::sequences[i].getGraph())[k];
                double y = (Test::sequences[j].getGraph())[k];
                max = (max < std::abs(x - y)) ? std::abs(x - y) : max;
            }
        Test::statresults.push_back(max);
    }

}






