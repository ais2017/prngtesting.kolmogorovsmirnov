#ifndef TEST_H
#define TEST_H

#include <cmath>
#include <exception>
#include "sequence.h"

using std::vector;

class Test
{
    double signlevel;
    double signvalue;
    vector<Sequence> sequences;
    vector<double> statresults;

public:
    Test(){}
    Test(double  signlevel);
    Test(double signlevel, vector<Sequence> sequences, vector<double> statresults);
    double getSignLevel();
    double getSignValue();
    vector<double> getStats();
    vector<Sequence> getSequences();
    Sequence getSequence(unsigned int index);
    void addSequence(const Sequence &seq);
    void deleteSequence(unsigned int index);
    void recalcStats();

    friend class TestTestclass;         //Test class
};

class test_sequence_index_error: public std::exception {
    virtual const char* what() const throw() {
        return "Access to nonexistent element";
    }
};

#endif // TEST_H
