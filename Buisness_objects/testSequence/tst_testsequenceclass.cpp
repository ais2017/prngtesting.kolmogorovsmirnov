#include <QtTest>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "sequence.h"

class TestSequenceclass : public QObject
{
    Q_OBJECT

public:
    TestSequenceclass();

private Q_SLOTS:
    void testConstructor();
    void testInitConstructor();
    void testInitConstructor_failed();
    void testCopyConstructor();
    void testGetSize_data();
    void testGetSize();
    void testGetGraph();
    void testBuildGraph();
    void testAccessToDataValue();
    void testAccessToDataValue_failed();
    void testAdd();
};

TestSequenceclass::TestSequenceclass() {
}

void TestSequenceclass::testConstructor() {
    Sequence seq0(0), seq1(8), seq2(10), seq3(16);

    QCOMPARE(seq0.data.size(), static_cast<unsigned long>(0));

    QCOMPARE(seq1.data.size(), static_cast<unsigned long>(256));
    for (int i = 0; i < 256; ++i)
        QCOMPARE(seq1.data[i], 0);

    QCOMPARE(seq2.data.size(), static_cast<unsigned long>(1024));
    for (int i = 0; i < 1024; ++i)
        QCOMPARE(seq2.data[i], 0);

    QCOMPARE(seq3.data.size(), static_cast<unsigned long>(65536));
    for (int i = 0; i < 65536; ++i)
        QCOMPARE(seq3.data[i], 0);
}

void TestSequenceclass::testInitConstructor()
{
    vector<int> data, data1(256), data2(65536);
    vector<double> graph, graph1(256), graph2(65536);

    srand(time(NULL));
    for (int i = 0; i < 256; ++i) {
        data1[i] = rand();
        graph1[i] = static_cast<double>(rand() % 1000) / 1000;
    }
    for (int i = 0; i < 65536; ++i) {
        data2[i] = rand();
        graph2[i] = static_cast<double>(rand() % 1000) / 1000;
    }

    Sequence seq(data, graph), seq1(data1, graph1), seq2(data2, graph2);
    QVERIFY(seq.data == data && seq.graph == graph);
    QVERIFY(seq1.data == data1 && seq1.graph == graph1);
    QVERIFY(seq2.data == data2 && seq2.graph == graph2);

}


void TestSequenceclass::testInitConstructor_failed() {
    vector<int> data(8);
    vector<double> graph(64);
    QVERIFY_EXCEPTION_THROWN(Sequence(data, graph), two_size_error);
}

void TestSequenceclass::testCopyConstructor() {
    vector<int> data, data1(64), data2(65536);
    vector<double> graph, graph1(64), graph2(65536);

    srand(time(NULL));
    for (int i = 0; i < 64; ++i) {
        data1[i] = rand();
        graph1[i] = static_cast<double>(rand() % 1000) / 1000;
    }
    for (int i = 0; i < 65536; ++i) {
        data2[i] = rand();
        graph2[i] = static_cast<double>(rand() % 1000) / 1000;
    }


    Sequence seq(data, graph), seq1(data1, graph1), seq2(data2, graph2), seq3(data1, graph);
    Sequence s(seq), s1(seq1), s2(seq2), s3(seq3);
    QVERIFY(seq.data == s.data && seq.graph == s.graph);
    QVERIFY(seq1.data == s1.data && seq1.graph == s1.graph);
    QVERIFY(seq2.data == s2.data && seq2.graph == s2.graph);
    QVERIFY(seq3.data == s3.data && seq3.graph == s3.graph);
}

void TestSequenceclass::testGetSize_data() {
    QTest::addColumn<unsigned int>("seqsize");
    QTest::addColumn<unsigned int>("result");

    QTest::newRow("Zero size") << Sequence(0).getSize() << static_cast<unsigned int>(0);
    for (unsigned int i = 1; i < 16; ++i)
        QTest::newRow("Size") << Sequence(i).getSize() << static_cast<unsigned int>(1 << i);
}

void TestSequenceclass::testGetSize() {
    QFETCH(unsigned int, seqsize);
    QFETCH(unsigned int, result);

    QCOMPARE(seqsize, result);
}

void TestSequenceclass::testBuildGraph() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    vector<double> gr, graph({0.1, 0.13, 0.38, 0.6, 0.68, 0.74, 0.88, 1.0});

    Sequence seq0(data0, gr), seq(data, gr);
    seq0.buildGraph();
    seq.buildGraph();
    QCOMPARE(seq.graph.size(), graph.size());
    for (int i = 0; i < 8; ++i)
        QVERIFY(seq.graph[i] - graph[i] <= 0.001);
    QVERIFY(seq0.graph[0] == 0 && seq0.graph[1] == 0 && seq0.graph[2] == 0 && seq0.graph[3] == 0);
}

void TestSequenceclass::testGetGraph() {
    vector<int> data({10, 3, 25, 22, 8, 6, 14, 12});
    vector<double> graph({0.1, 0.13, 0.38, 0.6, 0.68, 0.74, 0.88, 1.0});

    Sequence seq(data, graph);
    QVERIFY(seq.getGraph() == graph);
}

void TestSequenceclass::testAccessToDataValue() {
    vector<int> data(65536);
    vector<double> graph;
    for (int i = 0; i < 65536; ++i)
        data[i] = rand();

    Sequence seq(data, graph);
    for (unsigned int i = 0; i < 65536; ++i)
        QCOMPARE(seq[i], data[i]);
}

void TestSequenceclass::testAccessToDataValue_failed() {
    Sequence seq0(0), seq(8);
    QVERIFY_EXCEPTION_THROWN(seq0[0], sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(seq0[6], sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(seq[1000], sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(seq[3986], sequence_index_error);
}

void TestSequenceclass::testAdd() {
    vector<int> data({1, 0, 0, 0, 2, 0, 0, 1});
    Sequence seq(3), seq1(data, vector<double>());
    seq.add(4);
    seq.add(4);
    seq.add(7);
    seq.add(0);
    seq.add(90);
    QVERIFY(seq.data == seq1.data);
}

QTEST_APPLESS_MAIN(TestSequenceclass)

#include "tst_testsequenceclass.moc"
