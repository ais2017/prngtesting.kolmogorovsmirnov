#-------------------------------------------------
#
# Project created by QtCreator 2018-11-01T21:41:51
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_testsequenceclass
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11
LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LDFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

TEMPLATE = app

INCLUDEPATH += "../"
SOURCES += tst_testsequenceclass.cpp \
    ../sequence.cpp
HEADERS += \
    ../sequence.h
DEFINES += SRCDIR=\\\"$$PWD/\\\"
