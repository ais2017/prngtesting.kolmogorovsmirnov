#include "sequence.h"

Sequence::Sequence(unsigned int size) {      //size - amount of bits in entered numbers
    //Size of data vector is 2^size, using bit shift for fast multiplying
    int datasize = (size != 0)?1 << size:0;
    Sequence::data.resize(datasize, 0);
}

Sequence::Sequence(vector<int> data, vector<double> graph) {
    if (data.size() != graph.size() && graph.size() != 0)
        throw two_size_error();
    else {
        Sequence::data = data;
        Sequence::graph = graph;
    }
}

Sequence::Sequence(const Sequence &seq) {
    Sequence::data = seq.data;
    Sequence::graph = seq.graph;
}

unsigned int Sequence::getSize() {
    return Sequence::data.size();
}

vector<double> Sequence::getGraph() {
    return Sequence::graph;
}

void Sequence::buildGraph() {
    Sequence::graph.resize(Sequence::data.size(), 0);

    //Get sum of all data elements
    int sum = 0;
    for (unsigned int i = 0; i < Sequence::data.size(); ++i)
        sum += Sequence::data[i];

    //Calculate frequency with accumulation
    if (sum != 0) {
        Sequence::graph[0] = double(Sequence::data[0]) / double(sum);
        for (unsigned int i = 1; i < Sequence::data.size(); ++i)
            graph[i] = (double(Sequence::data[i]) / double(sum)) + Sequence::graph[i - 1];
    }
}

int Sequence::operator[](unsigned int index) {
    if (index >= data.size())
        throw sequence_index_error();
    return Sequence::data[index];
}

void Sequence::add(int value) {
    if (0 <= value && static_cast<unsigned int>(value) < Sequence::data.size())
        data[value]++;
}















